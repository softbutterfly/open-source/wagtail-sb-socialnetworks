![Community-Project](https://gitlab.com/softbutterfly/open-source/open-source-office/-/raw/master/banners/softbutterfly-open-source--banner--community-project.png)

![PyPI - Supported versions](https://img.shields.io/pypi/pyversions/wagtail-sb-socialnetworks)
![PyPI - Package version](https://img.shields.io/pypi/v/wagtail-sb-socialnetworks)
![PyPI - Downloads](https://img.shields.io/pypi/dm/wagtail-sb-socialnetworks)
![PyPI - MIT License](https://img.shields.io/pypi/l/wagtail-sb-socialnetworks)

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/329484ea99434c708f5c8dbd611f3d35)](https://app.codacy.com/gl/softbutterfly/wagtail-sb-socialnetworks/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

# Wagtail Social Networks

Wagtail package to manage sites social network profiles.

## Requirements

- Python 3.8, 3.9, 3.10

## Install

```bash
pip install wagtail-sb-socialnetworks
```

## Usage

Add `wagtail.contrib.settings` and `wagtail_sb_socialnetworks` to your `INSTALLED_APPS` settings

```
INSTALLED_APPS = [
  # ...
  "wagtail.contrib.settings",
  "wagtail_sb_socialnetworks",
  # ...
]
```

## Docs

- [Ejemplos](https://gitlab.com/softbutterfly/open-source/wagtail-sb-socialnetworks/-/wikis)
- [Wiki](https://gitlab.com/softbutterfly/open-source/wagtail-sb-socialnetworks/-/wikis)

## Changelog

All changes to versions of this library are listed in the [change history](CHANGELOG.md).

## Development

Check out our [contribution guide](CONTRIBUTING.md).

## Contributors

See the list of contributors [here](https://gitlab.com/softbutterfly/open-source/wagtail-sb-socialnetworks/-/graphs/develop).
